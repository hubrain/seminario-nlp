# Seminario de NLP

El propósito de este seminario es aprender y discutir temas de procesamiento de lenguaje natural. Para esto se seguirá el [curso de Christopher Manning de 2019](https://web.stanford.edu/class/archive/cs/cs224n/cs224n.1194/index.html#schedule).

# Sesiones


|  Fecha  |   Temas   |  A cargo  | Recursos |
| :-----  | :-------- | :-------  | :------- |
| 10-02-2021 | Introducción a git, gitlab y álgebra lineal | Vladimir (@VolodyaCO) y Juan Flórez (@trucupey) | [[vídeo](https://youtu.be/1f8KLfSWNog)] |
| 17-02-2021 | Introducción a probabilidad y a Pytorch (primera parte)| Juan Floréz (@trucupey) y Vladimir (@VolodyaCO)| [[vídeo](https://youtu.be/GyGQLDh6YJM)] [[notebook](https://gitlab.com/hubrain/seminario-nlp/-/blob/master/Sesiones/code/1%20-%20Introducci%C3%B3n%20a%20Pytorch/Pytorch_intro.ipynb)]|
| 24-02-2021 | Introducción a Pytorch (Continuación) y preprocesamiento | Juan Guillermo (@jgduenasl) y Vladimir (@VolodyaCO)|[[vídeo](https://youtu.be/KyRqoosSV7w)] [[notebook](https://gitlab.com/hubrain/seminario-nlp/-/blob/master/Sesiones/code/2%20-%20Preprocesamiento/preprocesamiento.ipynb)]|
| 10-03-2021 | Word2Vec | Juan Guillermo (@jgduenasl) | [[vídeo](https://youtu.be/wDSP7RZ4e08)] |
| 17-03-2021 | Word2Vec | Juan Guillermo (@jgduenasl) y Diana (@DianaCabrera) | [[vídeo (teoría)](https://youtu.be/a1DWTR2B8jQ)] [[vídeo (práctica)](https://youtu.be/6TFLH4nasEA)] [[notebook-1](https://gitlab.com/hubrain/seminario-nlp/-/blob/master/Sesiones/code/3%20-%20Word2Vec/Gensim_word_vector_visualization.ipynb)] [[notebook-2](https://gitlab.com/hubrain/seminario-nlp/-/blob/master/Sesiones/code/3%20-%20Word2Vec/Models_Baseline_Pytorch_W2V.ipynb)] |
| 24-03-2021 | Negative Sampling| Vladimir (@VolodyaCO)| [[vídeo](https://youtu.be/F2idJncT5II)] |
| 07-04-2021 | Glove | Santiago (@stoledoc), Diana (@DianaCabrera)|[[vídeo](https://youtu.be/n3Nf5QJ4j6M)] |
| 21-04-2021 | Word window Class|Juan Flórez (@trucupey)| [[vídeo 1](https://youtu.be/LSIFd4qYAjA)] [[vídeo 2](https://youtu.be/6A1qin0XkHY)] |
| 26-05-2021 | Language Models & Recurrent Neural Networks (@nparraa10)| [[vídeo 1](https://youtu.be/GJ9NJV8QwLE)] [[vídeo 2](https://youtu.be/f1K55_GUGi8)] |
| 02-06-2021| Linguistic Structure| Victor (@PrxSaa) y Vladimir (@VolodyaCO)| [[vídeo 1](https://youtu.be/kS3ec3Wl9jY)] [[vídeo 2](https://youtu.be/tPSQNgjuaAI)] |
| 16-06-2021| Vanishing gradients| Nicolas||
| 20-06-2021| Machine translation| Leonardo, Juan Flórez||
| 14-07-2021| Practical tips| Carlos G||

# Bibliografía

Disponible en [bibliografia.md](bibliografia.md)
